﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ActorSharp.Core
{
    public abstract class ActorRef
    {
    }

    public abstract class ActorRef<TActor> : ActorRef
    {
        public abstract void Send(IMessage<TActor> message);
        public abstract Task Call(ICall<TActor> call);
        public abstract Task<TResult> Call<TResult>(ICall<TActor, TResult> call);
        //public abstract IAwaitable CallSlim(ICall<TActor> call);
        //public abstract IAwaitable<TResult> CallSlim<TResult>(ICall<TActor, TResult> call);
        public abstract Task Call(IAsyncCall<TActor> call);
        public abstract Task<TResult> Call<TResult>(IAsyncCall<TActor, TResult> call);

        //public abstract bool IsInActorContext { get; }

        //public abstract void Send(Action<TActor> method);
        //public abstract Task Call(Action<TActor> method);
        //public abstract Task Call(Func<TActor, Task> method);
        //public abstract Task<TResult> Call<TResult>(Func<TActor, TResult> method);
        //public abstract Task<TResult> Call<TResult>(Func<TActor, Task<TResult>> method);
        //public abstract void SendChannel<T>(Channel<T> channel, Action<TActor, Channel<T>> actorMethod);
        //public abstract Task OpenChannel<T>(Channel<T> channel, Action<TActor, Channel<T>> actorMethod);
        //public abstract Task<TResult> OpenChannel<T, TResult>(Channel<T> channel, Func<TActor, Channel<T>, TResult> actorMethod);
    }
}
