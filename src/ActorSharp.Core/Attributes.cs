﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActorSharp.Core
{
    [AttributeUsage(AttributeTargets.Interface, AllowMultiple = false, Inherited = false)]
    public sealed class ActorContractAttribute : Attribute
    {
        public ActorContractAttribute(Type actorClass)
        {
            ActorClass = actorClass ?? throw new ArgumentNullException("actorClass");
        }

        public Type ActorClass { get; }
    }

    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public sealed class ActorBindingAttribute : Attribute
    {
        public string TargetMethod { get; }
        public bool IsOneWay { get; }
        //public bool IsBlocking { get; }
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public sealed class SelfReferenceAttribute : Attribute
    {
    }
}
