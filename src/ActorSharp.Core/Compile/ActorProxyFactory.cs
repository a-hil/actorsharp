﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Threading;

namespace ActorSharp.Core
{
    public abstract class ActorProxyFactory<TContract>
    {
        public static ActorProxyFactory<TContract> Instance { get; }

        public abstract TContract CreateLocalProxy(ActorRef actorRef);
        public abstract TContract SpawnActor(SynchronizationContext context);

        static ActorProxyFactory()
        {
            var contractTypeInfo = typeof(TContract);
            var contractAssembly = contractTypeInfo.Assembly;
            var contractNamespace = contractTypeInfo.Namespace;

            var contractAttribute = contractTypeInfo.GetCustomAttribute<ActorContractAttribute>();
            if (contractAttribute == null)
            {
                Instance = new InvalidProxyFactory<TContract>("Actor contract is not market with ActorContractAttribute!");
                return;
            }

            Assembly interopAssembly;

            try
            {
                interopAssembly = CodeGenManager.GetIneropAssembly(contractAssembly);
            }
            catch (Exception ex)
            {
                Instance = new InvalidProxyFactory<TContract>("Faied to load/generate interop assembly: " + ex.Message, ex);
                return;
            }

            var factoryClassFullName = contractNamespace + "." + Naming.GetProxyFactoryName(contractTypeInfo);
            var factoryClassType = interopAssembly.GetType(factoryClassFullName);
            if (factoryClassType == null)
            {
                Instance = new InvalidProxyFactory<TContract>("Interop assembly does not contain factory class '" + factoryClassType + "!");
                return;
            }

            try
            {
                Instance = (ActorProxyFactory<TContract>)Activator.CreateInstance(factoryClassType);
            }
            catch (Exception ex)
            {
                Instance = new InvalidProxyFactory<TContract>("Cannot create instance of factory class '" + factoryClassType + ": " + ex.Message, ex);
            }
        }
    }

    internal class InvalidProxyFactory<TContract> : ActorProxyFactory<TContract>
    {
        private string _errorMessage;
        private Exception _innerException;

        public InvalidProxyFactory(string message, Exception innerException = null)
        {
            _errorMessage = message;
            _innerException = innerException;
        }

        public override TContract CreateLocalProxy(ActorRef actorRef)
        {
            throw new ActorContractException(_errorMessage, _innerException);
        }

        public override TContract SpawnActor(SynchronizationContext context)
        {   
            throw new ActorContractException(_errorMessage, _innerException);
        }
    }
}
