﻿using System;
using System.Collections.Generic;
using System.Text;
using System.CodeDom;
using System.Reflection;
using System.CodeDom.Compiler;
using Microsoft.CSharp;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;

namespace ActorSharp.Core
{
    public class CodeBuilder
    {
        private List<CodeCompileUnit> _units = new List<CodeCompileUnit>();
        private List<Assembly> _actorAssemblies = new List<Assembly>();

        public event Action<Exception> OnContractError;

        public bool GenerateErrorFactories { get; set; }

        public void AddAssembly(Assembly actorAssembly)
        {
            foreach (var type in actorAssembly.GetTypes())
            {
                var contractAttribute = type.GetCustomAttribute<ActorContractAttribute>();
                if (contractAttribute != null)
                    AddContract(type, contractAttribute);
            }

            _actorAssemblies.Add(actorAssembly);
        }

        public void AddContract(Type actorContractType, ActorContractAttribute attirubte)
        {
            string errorMessage = null;

            try
            {
                GenerateProxy(new LocalProxyBuilder(), actorContractType, attirubte);
            }
            catch (Exception ex)
            {
                OnContractError?.Invoke(ex);

                if (!GenerateErrorFactories)
                    return;

                errorMessage = ex.Message;
            }

            GenerateFactory(actorContractType, attirubte.ActorClass, errorMessage);
        }

        private void GenerateProxy(IProxyBuilder builder, Type actorContractType, ActorContractAttribute contractMeta)
        {
            var nameSpace = actorContractType.Namespace;
            var actorClass = contractMeta.ActorClass;

            if (actorClass == null)
                throw new ActorContractException("ActorClass property is not set in ActorContractAttribute!");

            builder.Init(nameSpace, actorContractType, actorClass);

            var methodsToImplement = actorContractType.GetMethods();

            foreach (var method in methodsToImplement)
                AddContractMethod(builder, method, actorClass);

            _units.Add(builder.Complete());
        }

        private void AddContractMethod(IProxyBuilder builder, MethodInfo contractMethod, Type actorClass)
        {
            IProxyInvokeBuilder methodBuilder;
            var methodParams = contractMethod.GetParameters();

            var targetMethodOverloads = actorClass.GetOverloads(contractMethod.Name).ToArray();

            if (targetMethodOverloads.Length == 0)
                throw new ActorContractException("Cannot bind contract method '" + contractMethod.Name + "': there is no corresponding method in actor class!");

            if (targetMethodOverloads.Length > 1)
                throw new ActorContractException("Actor class '" + actorClass.Name + "' has two ore more methods with name '" + contractMethod.Name + "'. Overloads are not supported!");

            var targetMethod = targetMethodOverloads[0];

            var targetReturnType = targetMethod.ReturnType;
            var contractReturnType = contractMethod.ReturnType;

            if (contractReturnType == typeof(void) && targetReturnType == typeof(void))
                methodBuilder = builder.AddNotification(contractMethod.Name);
            else if (IsTask(contractReturnType) && targetReturnType == typeof(void))
                methodBuilder = builder.AddMethod(contractMethod.Name);
            else if (IsResultTask(contractReturnType) && !IsAnyKindTask(targetReturnType) && contractReturnType.GetGenericArguments()[0] == targetReturnType)
                methodBuilder = builder.AddFunction(contractMethod.Name, targetReturnType);
            else if (IsTask(targetReturnType) && IsTask(contractReturnType))
                methodBuilder = builder.AddAsyncMethod(contractMethod.Name);
            else if (IsResultTask(targetReturnType) && targetReturnType == contractReturnType)
                methodBuilder = builder.AddAsyncFunction(contractMethod.Name, contractReturnType.GetGenericArguments().FirstOrDefault());
            else
                throw new ActorContractException("Cannot bind contract method '" + contractMethod.Name + "': Return type does not match!");

            var targetParams = targetMethod.GetParameters();
            var contractParams = contractMethod.GetParameters();

            if (targetParams.Length != contractParams.Length)
                throw new ActorContractException("Cannot bind contract method '" + contractMethod.Name + "': parameters count does not match!");

            for (int i = 0; i < targetParams.Length; i++)
            {
                var targetParam = targetParams[i];
                var contractParam = contractParams[i];

                if (targetParam.ParameterType != contractParam.ParameterType)
                    throw new ActorContractException("Cannot bind contract method '" + contractMethod.Name + "': type of parameter '" + contractParam.Name + " does not match!");

                methodBuilder.AddPlainParameter(contractParam.ParameterType, contractParam.Name);
            }

            methodBuilder.Complete();
        }

        private bool IsTask(Type type)
        {
            return type == typeof(Task);
        }

        private bool IsAnyKindTask(Type type)
        {
            return type.IsSubclassOf(typeof(Task));
        }

        private bool IsResultTask(Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Task<>);
        }

        private void GenerateFactory(Type actorContractType, Type actorType, string errorMessage)
        {
            var unit = new CodeCompileUnit();
            var nameSpace = new CodeNamespace(actorContractType.Namespace);
            var factoryClass = new CodeTypeDeclaration(Naming.GetProxyFactoryName(actorContractType));

            factoryClass.BaseTypes.Add(typeof(ActorProxyFactory<>).CreateGenericReference(actorContractType));

            unit.Namespaces.Add(nameSpace);
            nameSpace.Types.Add(factoryClass);

            var proxyFactoryMethod = new CodeMemberMethod();
            proxyFactoryMethod.Name = Naming.CreateProxyMethodName;
            proxyFactoryMethod.Attributes = MemberAttributes.Public | MemberAttributes.Override;
            proxyFactoryMethod.Parameters.Add(new CodeParameterDeclarationExpression(typeof(ActorRef), "actorRef"));
            proxyFactoryMethod.ReturnType = new CodeTypeReference(actorContractType);

            var actorSpawnMethod = new CodeMemberMethod();
            actorSpawnMethod.Name = Naming.SpwanActroMethodName;
            actorSpawnMethod.Attributes = MemberAttributes.Public | MemberAttributes.Override;
            actorSpawnMethod.Parameters.Add(new CodeParameterDeclarationExpression(typeof(SynchronizationContext), "context"));
            actorSpawnMethod.ReturnType = new CodeTypeReference(actorContractType);


            //var refFactoryMethod = new CodeMemberMethod();
            //refFactoryMethod.Name = "CreateLocalRef";
            //refFactoryMethod.Attributes = MemberAttributes.Public | MemberAttributes.Override;
            //refFactoryMethod.Parameters.Add(new CodeParameterDeclarationExpression(typeof(IContextFactory), "factory"));
            //refFactoryMethod.ReturnType = typeof(ActorRef<>).CreateGenericReference(actorContractType);

            if (errorMessage == null)
            {
                var actorRefType = typeof(ActorRef<>).CreateGenericReference(actorType);
                var localRefType = typeof(LocalRef<>).CreateGenericReference(actorType);
                var localProxyTypeName = Naming.GetLocalProxyName(actorContractType);

                var proxyNewStatement = new CodeObjectCreateExpression(localProxyTypeName);
                proxyNewStatement.Parameters.Add(new CodeCastExpression(actorRefType, new CodeVariableReferenceExpression("actorRef")));
                proxyFactoryMethod.Statements.Add(new CodeMethodReturnStatement(proxyNewStatement));

                var actorVar = new CodeVariableDeclarationStatement(actorType, "actorInstance", new CodeObjectCreateExpression(actorType));
                actorSpawnMethod.Statements.Add(actorVar);
                var refCreation = new CodeObjectCreateExpression(localRefType, new CodeVariableReferenceExpression("actorInstance"), new CodeVariableReferenceExpression("context"));
                var localRefVar = new CodeVariableDeclarationStatement(actorRefType, "actorRef", refCreation);
                actorSpawnMethod.Statements.Add(localRefVar);
                var spawnProxyCreation = new CodeObjectCreateExpression(localProxyTypeName, new CodeVariableReferenceExpression("actorRef"));
                actorSpawnMethod.Statements.Add(new CodeMethodReturnStatement(spawnProxyCreation));

                //var refNewStatement = new CodeObjectCreateExpression(typeof(LocalRef<,>).CreateGenericReference(actorType, actorContractType));
                //refNewStatement.Parameters.Add(new CodeVariableReferenceExpression("factory"));
                //refFactoryMethod.Statements.Add(new CodeMethodReturnStatement(refNewStatement));
            }
            else
            {
                var exceptionNewStatement = new CodeObjectCreateExpression(typeof(ActorContractException), new CodePrimitiveExpression(errorMessage));
                var throwStatement = new CodeThrowExceptionStatement(exceptionNewStatement);
                proxyFactoryMethod.Statements.Add(throwStatement);
                actorSpawnMethod.Statements.Add(throwStatement);
                //refFactoryMethod.Statements.Add(throwStatement);
            }

            factoryClass.Members.Add(proxyFactoryMethod);
            factoryClass.Members.Add(actorSpawnMethod);
            //factoryClass.Members.Add(refFactoryMethod);

            _units.Add(unit);
        }

        public CompilerResults BuildAssembly(string outputFilePath = null)
        {
            var provider = new CSharpCodeProvider();

            var cParams = new CompilerParameters();
            foreach (var contractAssembly in _actorAssemblies)
                cParams.ReferencedAssemblies.Add(contractAssembly.Location);
            cParams.ReferencedAssemblies.Add(Assembly.GetExecutingAssembly().Location);

#if KEEP_GEN_FILES

            cParams.TempFiles.KeepFiles = true;
            var genFilesFolder = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "GenFiles");
            var dir = Directory.CreateDirectory(genFilesFolder);
            foreach (var file in dir.GetFiles()) file.Delete();
#endif
            if (outputFilePath != null)
                cParams.OutputAssembly = outputFilePath;
            else
                cParams.GenerateInMemory = true;

            var result =  provider.CompileAssemblyFromDom(cParams, _units.ToArray());

#if KEEP_GEN_FILES

            foreach (var tmpFile in cParams.TempFiles)
            {
                var tmpFilePath = (string)tmpFile;
                var tmpFileName = Path.GetFileName(tmpFilePath);

                try
                {
                    File.Copy(tmpFilePath, Path.Combine(genFilesFolder, tmpFileName));
                }
                catch { }
            }
#endif
            return result;
        }
    }
}

