﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ActorSharp
{
    internal static class CodeDomHelper
    {
        public static CodeTypeReference CreateGenericReference(this Type classType, params Type[] argumentTypes)
        {
            return new CodeTypeReference(classType.FullName,
                argumentTypes.Select(t => new CodeTypeReference(t.FullName)).ToArray());
        }

        public static CodeTypeReference CreateGenericReference(this Type classType, params string[] argumentTypes)
        {
            return new CodeTypeReference(classType.FullName,
                argumentTypes.Select(t => new CodeTypeReference(t)).ToArray());
        }

        public static IEnumerable<MethodInfo> GetOverloads(this Type typeInfo, string name)
        {
            foreach (var m in typeInfo.GetMethods())
            {
                if (m.Name == name)
                    yield return m;
            }
        }
    }
}
