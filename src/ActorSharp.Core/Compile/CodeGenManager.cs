﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace ActorSharp.Core
{
    internal class CodeGenManager
    {
        public const string AssemblySuffix = "AsGen";

        private static readonly Dictionary<string, Assembly> _genAssemblies = new Dictionary<string, Assembly>();

        public static Assembly GetIneropAssembly(Assembly contractAssembly)
        {
            lock (_genAssemblies)
            {
                Assembly interopAssembly;
                if (!_genAssemblies.TryGetValue(contractAssembly.FullName, out interopAssembly))
                {
                    interopAssembly = LoadOrGenerate(contractAssembly);
                    _genAssemblies.Add(contractAssembly.FullName, interopAssembly);
                }
                return interopAssembly;
            }
        }

        private static Assembly LoadOrGenerate(Assembly contractAssembly)
        {
            var interopAssemblyName = Naming.GetProxyAssemblyName(contractAssembly);

            try
            {
                return Assembly.Load(interopAssemblyName);
            }
            catch (System.IO.FileNotFoundException)
            {
                return Generate(contractAssembly);
            }
        }

        private static Assembly Generate(Assembly contractAssembly)
        {
            var compilerTool = new CodeBuilder();
            compilerTool.GenerateErrorFactories = true;
            compilerTool.AddAssembly(contractAssembly);
            var result = compilerTool.BuildAssembly();

            if (result.Errors.HasErrors)
            {
                var msgBuilder = new StringBuilder();
                msgBuilder.AppendLine("Failed to compile proxies! Errors:");
                foreach (CompilerError err in result.Errors)
                    msgBuilder.AppendLine(err.ErrorText);

                throw new ActorContractException(msgBuilder.ToString());
            }

            return result.CompiledAssembly;
        }
    }
}
