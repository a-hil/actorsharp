﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActorSharp
{
    public interface IProxyBuilder
    {
        void Init(string nameSpace, Type contract, Type actorClass);
        IProxyInvokeBuilder AddNotification(string name);
        IProxyInvokeBuilder AddMethod(string name);
        IProxyInvokeBuilder AddFunction(string name, Type returnType);
        IProxyInvokeBuilder AddAsyncMethod(string name);
        IProxyInvokeBuilder AddAsyncFunction(string name, Type returnType);
        CodeCompileUnit Complete();
    }

    public interface IProxyInvokeBuilder
    {
        void AddPlainParameter(Type paramType, string paramName);
        void Complete();
    }
}
