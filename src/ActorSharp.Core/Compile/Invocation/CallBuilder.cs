﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActorSharp.Core.Compile.Invocation
{
    internal class CallBuilder : InvokeBuilder
    {
        private CodeMemberMethod _msgInvokeMethod;
        private CodeMethodInvokeExpression _actorMethodInvocation;

        protected override CodeTypeReference GetBaseInterface()
        {
            if (ReturnType == null)
                return typeof(ICall<>).CreateGenericReference(ActorType);
            else
                return typeof(ICall<,>).CreateGenericReference(ActorType, ReturnType);
        }

        protected override CodeTypeReference GetInvokeReturnType()
        {
            if (ReturnType == null)
                return new CodeTypeReference(typeof(Task));
            else
                return typeof(Task<>).CreateGenericReference(ReturnType);
        }

        protected override void ImplementMessageBaseClass()
        {
            _msgInvokeMethod = new CodeMemberMethod();
            _msgInvokeMethod.Name = nameof(IMessage<object>.Invoke);
            _msgInvokeMethod.ReturnType = new CodeTypeReference(ReturnType ?? typeof(void));
            _msgInvokeMethod.Attributes = MemberAttributes.Public | MemberAttributes.Final;
            _msgInvokeMethod.Parameters.Add(new CodeParameterDeclarationExpression(ActorType, "actor"));

            _actorMethodInvocation = new CodeMethodInvokeExpression(new CodeVariableReferenceExpression("actor"), TargetMethodName);
            if (ReturnType == null)
                _msgInvokeMethod.Statements.Add(_actorMethodInvocation);
            else
                _msgInvokeMethod.Statements.Add(new CodeMethodReturnStatement(_actorMethodInvocation));

            MessageClass.Members.Add(_msgInvokeMethod);
        }

        protected override void GenerateMessagePassStatement()
        {
            var refFieldAccessor = new CodeFieldReferenceExpression(new CodeThisReferenceExpression(), Naming.ProxyActorRefFieldName);
            var actorRefInvoke = new CodeMethodInvokeExpression(
                refFieldAccessor,
                nameof(ActorRef<object>.Call),
                new CodeVariableReferenceExpression(Naming.LocalMessageVariableName));
            InvokeMethod.Statements.Add(new CodeMethodReturnStatement(actorRefInvoke));
        }

        protected override void AddInvokeParam(Type paramType, string paramName)
        {
            _actorMethodInvocation.Parameters.Add(new CodeFieldReferenceExpression(new CodeThisReferenceExpression(), paramName));
        }
    }
}
