﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActorSharp.Core.Compile.Invocation
{
    internal abstract class InvokeBuilder : IProxyInvokeBuilder
    {
        protected CodeTypeDeclaration ProxyClass { get; private set; }
        protected Type ActorType { get; private set; }
        protected string MethodName { get; private set; }
        protected string TargetMethodName { get; private set; }
        protected Type ReturnType { get; private set; }

        protected CodeTypeDeclaration MessageClass { get; private set; }
        protected CodeMemberMethod InvokeMethod { get; private set; }

        protected abstract CodeTypeReference GetBaseInterface();
        //protected abstract void Init();
        protected abstract void ImplementMessageBaseClass();
        protected abstract void AddInvokeParam(Type paramType, string paramName);
        protected abstract void GenerateMessagePassStatement();
        protected abstract CodeTypeReference GetInvokeReturnType();

        public void Init(CodeTypeDeclaration proxyClass, Type actorClass, string methodName, string targetMethodName, Type returnType)
        {
            ProxyClass = proxyClass;
            ActorType = actorClass;
            MethodName = methodName;
            TargetMethodName = targetMethodName;
            ReturnType = returnType;

            GenerateMessageClass();
            GenerateInvokeMethod();
        }

        private void GenerateMessageClass()
        {
            MessageClass = new CodeTypeDeclaration("Message_" + MethodName);
            MessageClass.BaseTypes.Add(GetBaseInterface());
            //_messageClass.Members.Add(invokeMethod);

            ImplementMessageBaseClass();

            ProxyClass.Members.Add(MessageClass);
        }

        private void GenerateInvokeMethod()
        {
            InvokeMethod = new CodeMemberMethod();
            InvokeMethod.Name = MethodName;
            InvokeMethod.ReturnType = GetInvokeReturnType();
            InvokeMethod.Attributes = MemberAttributes.Public | MemberAttributes.Final;

            var messageCodeType = new CodeTypeReference(MessageClass.Name);
            var messageRef = new CodeVariableDeclarationStatement(messageCodeType, Naming.LocalMessageVariableName, new CodeObjectCreateExpression(messageCodeType));
            InvokeMethod.Statements.Add(messageRef);

            
        }

        public void AddPlainParameter(Type paramType, string paramName)
        {
            // add method parameter
            InvokeMethod.Parameters.Add(new CodeParameterDeclarationExpression(paramType, paramName));

            // create message field
            var messageProperty = new CodeMemberField();
            messageProperty.Name = paramName;
            messageProperty.Type = new CodeTypeReference(paramType);
            messageProperty.Attributes = MemberAttributes.Public | MemberAttributes.Final;
            MessageClass.Members.Add(messageProperty);

            // assign message field
            InvokeMethod.Statements.Add(new CodeAssignStatement(
                    new CodePropertyReferenceExpression(new CodeVariableReferenceExpression(Naming.LocalMessageVariableName), paramName),
                    new CodeVariableReferenceExpression(paramName)));

            AddInvokeParam(paramType, paramName);
        }

        public void Complete()
        {
            GenerateMessagePassStatement();

            ProxyClass.Members.Add(InvokeMethod);
        }
    }
}
