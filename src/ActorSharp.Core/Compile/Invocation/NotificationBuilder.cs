﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActorSharp.Core.Compile.Invocation
{
    internal class NotificationBuilder : InvokeBuilder
    {
        private CodeMemberMethod _msgInvokeMethod;
        private CodeMethodInvokeExpression _actorMethodInvocation;

        protected override CodeTypeReference GetBaseInterface()
        {
            return typeof(IMessage<>).CreateGenericReference(ActorType);
        }

        protected override CodeTypeReference GetInvokeReturnType()
        {
            return new CodeTypeReference(typeof(void));
        }

        protected override void ImplementMessageBaseClass()
        {
            _msgInvokeMethod = new CodeMemberMethod();
            _msgInvokeMethod.Name = nameof(IMessage<object>.Invoke);
            _msgInvokeMethod.ReturnType = new CodeTypeReference(typeof(void));
            _msgInvokeMethod.Attributes = MemberAttributes.Public | MemberAttributes.Final;
            _msgInvokeMethod.Parameters.Add(new CodeParameterDeclarationExpression(ActorType, "actor"));

            _actorMethodInvocation = new CodeMethodInvokeExpression(new CodeVariableReferenceExpression("actor"), TargetMethodName);
            _msgInvokeMethod.Statements.Add(_actorMethodInvocation);

            MessageClass.Members.Add(_msgInvokeMethod);
        }

        protected override void GenerateMessagePassStatement()
        {
            var refFieldAccessor = new CodeFieldReferenceExpression(new CodeThisReferenceExpression(), Naming.ProxyActorRefFieldName);
            var actorRefInvoke = new CodeMethodInvokeExpression(
                refFieldAccessor,
                nameof(ActorRef<object>.Send),
                new CodeVariableReferenceExpression(Naming.LocalMessageVariableName));
            InvokeMethod.Statements.Add(actorRefInvoke);
        }

        protected override void AddInvokeParam(Type paramType, string paramName)
        {
            _actorMethodInvocation.Parameters.Add(new CodeFieldReferenceExpression(new CodeThisReferenceExpression(), paramName));
        }
    }
}
