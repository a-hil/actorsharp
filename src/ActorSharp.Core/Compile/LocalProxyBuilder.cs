﻿using ActorSharp.Core.Compile.Invocation;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActorSharp.Core
{
    internal class LocalProxyBuilder : IProxyBuilder
    {
        private CodeCompileUnit _unit = new CodeCompileUnit();
        private CodeTypeDeclaration _proxyClass;
        private Type _actorClass;

        public void Init(string nameSpace, Type contract, Type actorClass)
        {
            _actorClass = actorClass;

            var actorRefType = typeof(ActorRef<>).CreateGenericReference(actorClass);
            var actorRefField = new CodeMemberField(actorRefType, Naming.ProxyActorRefFieldName);

            var initActorRefStatement = new CodeAssignStatement(
                new CodeFieldReferenceExpression(new CodeThisReferenceExpression(), Naming.ProxyActorRefFieldName),
                new CodeVariableReferenceExpression("actorRef"));

            var constructor = new CodeConstructor();
            constructor.Attributes = MemberAttributes.Public;
            constructor.Parameters.Add(new CodeParameterDeclarationExpression(actorRefType, "actorRef"));
            constructor.Statements.Add(initActorRefStatement);

            var proxyClassName = Naming.GetLocalProxyName(contract);
            _proxyClass = new CodeTypeDeclaration(proxyClassName);
            _proxyClass.BaseTypes.Add(new CodeTypeReference(contract));
            _proxyClass.Members.Add(constructor);
            _proxyClass.Members.Add(actorRefField);

            var codeNamespace = new CodeNamespace(nameSpace);
            codeNamespace.Types.Add(_proxyClass);

            _unit.Namespaces.Add(codeNamespace);
        }

        private IProxyInvokeBuilder Init(InvokeBuilder builder, string methodName, Type returnType)
        {
            builder.Init(_proxyClass, _actorClass, methodName, methodName, returnType);
            return builder;
        }

        public IProxyInvokeBuilder AddNotification(string name)
        {
            return Init(new NotificationBuilder(), name, null);
        }

        public IProxyInvokeBuilder AddMethod(string name)
        {
            return Init(new CallBuilder(), name, null);
        }

        public IProxyInvokeBuilder AddFunction(string name, Type returnType)
        {
            return Init(new CallBuilder(), name, returnType);
        }

        public IProxyInvokeBuilder AddAsyncMethod(string name)
        {
            return Init(new AsyncCallBuilder(), name, null);
        }

        public IProxyInvokeBuilder AddAsyncFunction(string name, Type returnType)
        {
            return Init(new AsyncCallBuilder(), name, returnType);
        }

        public CodeCompileUnit Complete()
        {
            return _unit;
        }
    }
}
