﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ActorSharp.Core
{
    internal static class Naming
    {
        public static AssemblyName GetProxyAssemblyName(Assembly contractAssembly)
        {
            var contractAssemblyName = new AssemblyName(contractAssembly.FullName);
            return new AssemblyName(contractAssemblyName.Name + ".AsGen");
        }

        public static string CreateProxyMethodName { get; } = nameof(ActorProxyFactory<object>.CreateLocalProxy);
        public static string SpwanActroMethodName { get; } = nameof(ActorProxyFactory<object>.SpawnActor);

        public static string ProxyActorRefFieldName { get; } = "aRef";
        public static string LocalMessageVariableName { get; } = "message";

        public static string GetProxyFactoryName(Type actorContractType)
        {
            return actorContractType.Name + "_GFactory";
        }

        public static string GetLocalProxyName(Type actorContractType)
        {
            return actorContractType.Name + "_GProxy";
        }

        public static string GetRemoteProxyName(Type actorContractType)
        {
            return actorContractType.Name + "_GRemProxy";
        }
    }
}
