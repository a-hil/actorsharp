﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActorSharp.Core
{
    public class ActorModelException : Exception
    {
        public ActorModelException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }

    public class ActorContractException : ActorModelException
    {
        public ActorContractException(string message, Exception innerException = null) : base(message, innerException)
        {
        }
    }

    public class ActorFaultException : ActorModelException
    {
        public ActorFaultException(string message, Exception innerException = null) : base(message, innerException)
        {
        }
    }
}
