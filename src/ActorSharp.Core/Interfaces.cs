﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ActorSharp
{
    #region .NET Async support

    public interface IAwaitable<T>
    {
        IAwaiter<T> GetAwaiter();
    }

    public interface IAwaiter<T> : INotifyCompletion
    {
        bool IsCompleted { get; }
        T GetResult();
    }

    public interface IAwaitable
    {
        IAwaiter GetAwaiter();
    }

    public interface IAwaiter : INotifyCompletion
    {
        bool IsCompleted { get; }
        void GetResult();
    }

    public interface IAsyncReader<T>
    {
        IAwaitable<bool> ReadNext();
        T Current { get; }
        IAwaitable Close(Exception ex = null);
    }

    #endregion

    public interface IContextFactory
    {
        SynchronizationContext CreateContext();
    }

    #region Invocation

    public interface IMessage<TActor>
    {
        void Invoke(TActor actor);
    }

    public interface ICall<TActor>
    {
        void Invoke(TActor actor);
    }

    public interface ICall<TActor, TResult>
    {
        TResult Invoke(TActor actor);
    }

    public interface IAsyncCall<TActor>
    {
        Task Invoke(TActor actor);
    }

    public interface IAsyncCall<TActor, TResult>
    {
        Task<TResult> Invoke(TActor actor);
    }

    #endregion
}
