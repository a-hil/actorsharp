﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActorSharp.Core
{
    

    //public abstract class Notification<TActor> : IMessage<TActor>
    //{
    //    public abstract void InvokeActorMethod(TActor actor);

    //    public void Invoke(TActor actor)
    //    {
    //        //try
    //        //{
    //        InvokeActorMethod(actor);
    //        //}
    //        //catch (Exception ex)
    //        //{
    //        //    // TO DO
    //        //}
    //    }
    //}

    //public abstract class MethodCall<TActor> : IMessage<TActor>
    //{
    //    public abstract void InvokeActorMethod(TActor actor);

    //    public void Invoke(TActor actor)
    //    {
    //        try
    //        {
    //            InvokeActorMethod(actor);
    //        }
    //        catch (Exception ex)
    //        {
    //            //TrySetException(ex);
    //        }
    //    }
    //}

    //public abstract class MethodCall<TActor, TResult> : TaskCompletionSource<TResult>, IMessage<TActor>
    //{
    //    public abstract TResult InvokeActorMethod(TActor actor);

    //    public void Invoke(TActor actor)
    //    {
    //        try
    //        {
    //            var result = InvokeActorMethod(actor);
    //            TrySetResult(result);
    //        }
    //        catch (Exception ex)
    //        {
    //            TrySetException(ex);
    //        }
    //    }
    //}

    //public abstract class AsyncMethodCall<TActor> : TaskCompletionSource<object>, IMessage<TActor>
    //{
    //    public abstract Task InvokeActorMethod(TActor actor);

    //    public void Invoke(TActor actor)
    //    {
    //        try
    //        {
    //            var task = InvokeActorMethod(actor);
    //            task.ContinueWith(HandleInvokeResults);
    //        }
    //        catch (Exception ex)
    //        {
    //            TrySetException(ex);
    //        }
    //    }

    //    private void HandleInvokeResults(Task asyncMethod)
    //    {
    //        if (asyncMethod.IsFaulted)
    //            TrySetException(asyncMethod.Exception);
    //        else if (asyncMethod.IsCanceled)
    //            TrySetCanceled();
    //        else
    //            TrySetResult(null);
    //    }
    //}

    //public abstract class AsyncMethodCall<TActor, TResult> : TaskCompletionSource<TResult>, IMessage<TActor>
    //{
    //    public abstract Task<TResult> InvokeActorMethod(TActor actor);

    //    public void Invoke(TActor actor)
    //    {
    //        try
    //        {
    //            var task = InvokeActorMethod(actor);
    //            task.ContinueWith(HandleInvokeResults);
    //        }
    //        catch (Exception ex)
    //        {
    //            TrySetException(ex);
    //        }
    //    }

    //    private void HandleInvokeResults(Task<TResult> asyncMethod)
    //    {
    //        if (asyncMethod.IsFaulted)
    //            TrySetException(asyncMethod.Exception);
    //        else if (asyncMethod.IsCanceled)
    //            TrySetCanceled();
    //        else
    //            TrySetResult(asyncMethod.Result);
    //    }
    //}
}
