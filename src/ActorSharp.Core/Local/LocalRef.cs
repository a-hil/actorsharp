﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ActorSharp.Core
{
    public class LocalRef<TActor> : ActorRef<TActor>
    //where TActor : Actor
    {
        public LocalRef(TActor actor, SynchronizationContext context)
        {
            ActorInstance = actor;
            ActorContext = context;
        }

        internal SynchronizationContext ActorContext { get; }
        internal TActor ActorInstance { get; }

        public override void Send(IMessage<TActor> message)
        {
            ActorContext.Post(ProcessMessage, message);
        }

        public override Task Call(ICall<TActor> call)
        {
            var item = new CallItem(call);
            ActorContext.Post(ExecInvokeItem, item);
            return item.Task;
        }

        public override Task<TResult> Call<TResult>(ICall<TActor, TResult> call)
        {
            var item = new CallItem<TResult>(call);
            ActorContext.Post(ExecInvokeItem, item);
            return item.Task;
        }

        public override Task Call(IAsyncCall<TActor> call)
        {
            var item = new AsyncCallItem(call);
            ActorContext.Post(ExecInvokeItem, item);
            return item.Task;
        }

        public override Task<TResult> Call<TResult>(IAsyncCall<TActor, TResult> call)
        {
            var item = new AsyncCallItem<TResult>(call);
            ActorContext.Post(ExecInvokeItem, item);
            return item.Task;
        }

        private void ProcessMessage(object arg)
        {
            try
            {
                var message = (IMessage<TActor>)arg;
                message.Invoke(ActorInstance);
            }
            catch (Exception ex)
            {
                // TO DO
            }
        }

        private void ExecInvokeItem(object task)
        {
            ((IInvokeItem)task).DoInvoke(ActorInstance);
        }

        //public override Task<TResult> Call<TResult>(Func<TActor, TResult> method)
        //{
        //    var task = new Task<TResult>(o => method((TActor)o), _actor);
        //    _context.Post(ExecTaskSync, task);
        //    return task;
        //}

        //private void ExecAction(object action)
        //{
        //    ((Action<TActor>)action).Invoke(_actor);
        //}

        //public override bool IsInActorContext => SynchronizationContext.Current == _context;

        /// <summary>
        /// Fire and forget style.
        /// </summary>
        /// <param name="method">Method to invoke in actor context.</param>
        //public override void Send(Action<TActor> method)
        //{
        //    _context.Post(ExecAction, method);
        //}

        //public override Task Call(Action<TActor> method)
        //{
        //    var task = new Task(o => method((TActor)o), _actor);
        //    _context.Post(ExecTaskSync, task);
        //    return task;
        //}



        //public override void SendChannel<T>(Channel<T> channel, Action<TActor, Channel<T>> actorMethod)
        //{
        //    if (channel.Dicrection == ChannelDirections.In)
        //    {
        //        var writer = new LocalChannelWriter<T>();
        //        channel.Init(writer);

        //        Action<TActor> task = a =>
        //        {
        //            var actorChannel = Channel.NewInput<T>();
        //            var reader = new LocalChannelReader<T>();
        //            reader.Init(writer);
        //            writer.Init(reader, channel.MaxPageSize);
        //            actorChannel.Init(reader);
        //            actorMethod(_actor, actorChannel);
        //        };

        //        _context.Post(ExecAction, task);
        //    }
        //    else if (channel.Dicrection == ChannelDirections.Out)
        //    {
        //        var reader = new LocalChannelReader<T>();
        //        channel.Init(reader);

        //        Action<TActor> task = a =>
        //        {
        //            var actorChannel = Channel.NewInput<T>();
        //            var writer = new LocalChannelWriter<T>();
        //            writer.Init(reader, channel.MaxPageSize);
        //            reader.Init(writer);
        //            actorChannel.Init(writer);
        //            actorMethod(_actor, actorChannel);
        //        };

        //        _context.Post(ExecAction, task);
        //    }
        //    else
        //        throw new NotImplementedException();
        //}

        //public override Task OpenChannel<T>(Channel<T> channel, Action<TActor, Channel<T>> actorMethod)
        //{
        //    if (channel.Dicrection == ChannelDirections.In)
        //    {
        //        var writer = new LocalChannelWriter<T>();
        //        channel.Init(writer);

        //        var task = new Task(() =>
        //        {
        //            var actorChannel = Channel.NewInput<T>();
        //            var reader = new LocalChannelReader<T>();
        //            reader.Init(writer);
        //            writer.Init(reader, channel.MaxPageSize);
        //            actorChannel.Init(reader);
        //            actorMethod(_actor, actorChannel);
        //        });

        //        _context.Post(ExecTaskSync, task);
        //        return task;
        //    }
        //    else if (channel.Dicrection == ChannelDirections.Out)
        //    {
        //        var reader = new LocalChannelReader<T>();
        //        channel.Init(reader);

        //        var task = new Task(() =>
        //        {
        //            var actorChannel = Channel.NewInput<T>();
        //            var writer = new LocalChannelWriter<T>();
        //            writer.Init(reader, channel.MaxPageSize);
        //            reader.Init(writer);
        //            actorChannel.Init(writer);
        //            actorMethod(_actor, actorChannel);
        //        });

        //        _context.Post(ExecTaskSync, task);
        //        return task;
        //    }
        //    else
        //        throw new NotImplementedException();
        //}

        //public override Task<TResult> OpenChannel<T, TResult>(Channel<T> channel, Func<TActor, Channel<T>, TResult> actorMethod)
        //{
        //    if (channel.Dicrection == ChannelDirections.In)
        //    {
        //        var writer = new LocalChannelWriter<T>();
        //        channel.Init(writer);

        //        var task = new Task<TResult>(() =>
        //        {
        //            var actorChannel = Channel.NewInput<T>();
        //            var reader = new LocalChannelReader<T>();
        //            reader.Init(writer);
        //            writer.Init(reader, channel.MaxPageSize);
        //            actorChannel.Init(reader);
        //            return actorMethod(_actor, actorChannel);
        //        });

        //        _context.Post(ExecTaskSync, task);
        //        return task;
        //    }
        //    else if (channel.Dicrection == ChannelDirections.Out)
        //    {
        //        var reader = new LocalChannelReader<T>();
        //        channel.Init(reader);

        //        var task = new Task<TResult>(() =>
        //        {
        //            var actorChannel = Channel.NewInput<T>();
        //            var writer = new LocalChannelWriter<T>();
        //            writer.Init(reader, channel.MaxPageSize);
        //            reader.Init(writer);
        //            actorChannel.Init(writer);
        //            return actorMethod(_actor, actorChannel);
        //        });

        //        _context.Post(ExecTaskSync, task);
        //        return task;
        //    }
        //    else
        //        throw new NotImplementedException();
        //}

        //public override bool Equals(object obj)
        //{
        //    var other = obj as LocalRef<TActor>;
        //    return other != null && ReferenceEquals(other._actor, _actor);
        //}

        //public override int GetHashCode()
        //{
        //    return _actor.GetHashCode();
        //}

        //internal override async Task<IChannelReader<T>> OpenRxChannel<T>(Action<TActor, IChannelWriter<T>> method, int pageSize)
        //{
        //    var thisSide = new LocalChannelReader<T>();

        //    var task = new Task(() =>
        //    {
        //        var oppositeSide = new LocalChannelWriter<T>();
        //        oppositeSide.Init(thisSide, pageSize);
        //        thisSide.Init(oppositeSide);
        //        method(_actor, oppositeSide);
        //    });

        //    Context.Post(ExecTaskSync, task);
        //    await task;
        //    return thisSide;
        //}

        //internal override async Task<IChannelWriter<T>> OpenTxChannel<T>(Action<TActor, IChannelReader<T>> method, int pageSize)
        //{
        //    var thisSide = new LocalChannelWriter<T>();

        //    var task = new Task(() =>
        //    {
        //        var oppositeSide = new LocalChannelReader<T>();
        //        oppositeSide.Init(thisSide);
        //        thisSide.Init(oppositeSide, pageSize);
        //        method(_actor, oppositeSide);
        //    });

        //    Context.Post(ExecTaskSync, task);
        //    await task;
        //    return thisSide;
        //}

        //    public override void PostMessage(object message)
        //    {
        //        var basicActor = _actor as Actor;

        //        if (basicActor != null)
        //            basicActor.PostMessage(message);
        //        else
        //            throw new InvalidOperationException("This actor does not support basic messaging pattern!");
        //    }

        //    private void ExecTaskSync(object task)
        //    {
        //        ((Task)task).RunSynchronously();
        //    }

        //    private void ExecAction(object action)
        //    {
        //        ((Action<TActor>)action).Invoke(_actor);
        //    }


        //}

        private interface IInvokeItem
        {
            void DoInvoke(TActor actor);
        }

        private class CallItem : TaskCompletionSource<object>, IInvokeItem
        {
            public CallItem(ICall<TActor> call)
            {
                Call = call;
            }

            public ICall<TActor> Call { get; }

            public void DoInvoke(TActor actor)
            {
                try
                {
                    Call.Invoke(actor);
                    SetResult(null);
                }
                catch (Exception ex)
                {
                    SetException(ex);
                }
            }
        }

        private class CallItem<TResult> : TaskCompletionSource<TResult>, IInvokeItem
        {
            public CallItem(ICall<TActor, TResult> call)
            {
                Call = call;
            }

            public ICall<TActor, TResult> Call { get; }

            public void DoInvoke(TActor actor)
            {
                try
                {
                    var result = Call.Invoke(actor);
                    SetResult(result);
                }
                catch (Exception ex)
                {
                    SetException(ex);
                }
            }
        }

        private class AsyncCallItem : TaskCompletionSource<object>, IInvokeItem
        {
            public AsyncCallItem(IAsyncCall<TActor> call)
            {
                Call = call;
            }

            public IAsyncCall<TActor> Call { get; }

            public void DoInvoke(TActor actor)
            {
                try
                {
                    var task = Call.Invoke(actor);
                    task.ContinueWith(Bind, TaskContinuationOptions.RunContinuationsAsynchronously);
                }
                catch (Exception ex)
                {
                    SetException(ex);
                }
            }

            private void Bind(Task t)
            {
                if (t.IsFaulted)
                    SetException(t.Exception);
                else
                    SetResult(null);
            }
        }

        private class AsyncCallItem<TResult> : TaskCompletionSource<TResult>, IInvokeItem
        {
            public AsyncCallItem(IAsyncCall<TActor, TResult> call)
            {
                Call = call;
            }

            public IAsyncCall<TActor, TResult> Call { get; }

            public void DoInvoke(TActor actor)
            {
                try
                {
                    var task = Call.Invoke(actor);
                    task.ContinueWith(Bind, TaskContinuationOptions.RunContinuationsAsynchronously);
                }
                catch (Exception ex)
                {
                    SetException(ex);
                }
            }

            private void Bind(Task<TResult> t)
            {
                if (t.IsFaulted)
                    SetException(t.Exception);
                else
                    SetResult(t.Result);
            }
        }

    }
}
