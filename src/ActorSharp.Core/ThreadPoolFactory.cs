﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ActorSharp.Core
{
    public class ThreadPoolFactory : IContextFactory
    {
        private int _messagesPerTask;

        public ThreadPoolFactory(int messagesPerTask)
        {
            _messagesPerTask = messagesPerTask;
        }

        public SynchronizationContext CreateContext()
        {
            return new PoolContext(_messagesPerTask);
        }
    }
}
