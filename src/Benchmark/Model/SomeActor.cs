﻿using ActorSharp.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Benchmark.Model
{
    [ActorContract(typeof(SomeActor))]
    public interface ISomeActorRef
    {
        void NotifySomething(int count, DateTime happenedAt);
        Task CallSomething(double p);
        Task CallSomethingAsync(double p);
        Task<int> DoSomething(double p);
        Task<int> DoSomethingAsync(double p);
    }

    public class SomeActor
    {
        public void NotifySomething(int count, DateTime happenedAt)
        {
        }

        public void CallSomething(double p)
        {
        }

        public Task CallSomethingAsync(double p)
        {
            return Task.Delay(1000);
        }

        public int DoSomething(double p)
        {
            return (int)p / 10;
        }

        public async Task<int> DoSomethingAsync(double p)
        {
            await Task.Delay(1000);
            return (int)p / 4;
        }
    }
}
