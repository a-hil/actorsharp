﻿using ActorSharp.Core;
using Benchmark.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Benchmark
{
    class Program
    {
        static void Main(string[] args)
        {
            ISomeActorRef sRef = Actor.Spawn<ISomeActorRef>();

            sRef.NotifySomething(25, DateTime.Now);
            sRef.CallSomething(1.2).Wait();
            var doResult = sRef.DoSomething(125.1).Result;
            sRef.CallSomethingAsync(100500).Wait();
            var doAsyncResult = sRef.DoSomethingAsync(26).Result;

            Console.Read();
        }
    }
}
